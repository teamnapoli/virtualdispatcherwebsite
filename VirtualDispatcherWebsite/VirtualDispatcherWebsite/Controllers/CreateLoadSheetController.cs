﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VirtualDispatcherWebsite.Models;
using VirtualDispatcher.Service;

namespace VirtualDispatcherWebsite.Controllers
{
    public class CreateLoadSheetController : Controller
    {
        private LoadSheetRepository loadSheetRepository = new LoadSheetRepository();
        private IFlightRouteService _flightRouteService;
        private ILoadSheetBagWeightService _loadsheetBagWeightService;
        private IPassengerWeightService _passengerWeightService;
        private IGetFleetWeightByRegistrationService _getFleetWeightByRegistrationNumber;

        public CreateLoadSheetController(IFlightRouteService flightRouteService, ILoadSheetBagWeightService loadsheetBagWeightService,
            IPassengerWeightService passengerWeightService,
            IGetFleetWeightByRegistrationService getFleetWeightByRegistrationService)
        {
            _flightRouteService = flightRouteService;
            _loadsheetBagWeightService = loadsheetBagWeightService;
            _passengerWeightService = passengerWeightService;
            _getFleetWeightByRegistrationNumber = getFleetWeightByRegistrationService;
        }

        // GET: CreateLoadSheet
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(CreateLoadSheetViewModel createLoadScheetModel)
        {
            // add server controls
            return RedirectToAction("LoadSheetResults", createLoadScheetModel);
            
        }

        public ActionResult LoadSheetResults(CreateLoadSheetViewModel createLoadScheetModel)
        {
            var flightRoute = _flightRouteService.Execute(createLoadScheetModel.FlightNumber);

            var fleetWeightData = _getFleetWeightByRegistrationNumber.Execute(createLoadScheetModel.AircraftRegistrationNumber);

            var loadSheetResultViewModel = new LoadSheetResultsViewModel();

            loadSheetResultViewModel.DryOperatingWeight = fleetWeightData.Weight +
                (createLoadScheetModel.Observer1 ? 90.1 : 0) +
                (createLoadScheetModel.Observer2 ? 90.1 : 0) +
                (createLoadScheetModel.Attendance5 ? 79.9 : 0) +
                (createLoadScheetModel.Attendance6 ? 79.9 : 0);

            loadSheetResultViewModel.TakeOffFuel = createLoadScheetModel.RampFuel - createLoadScheetModel.TaxiFuel; //TODO: aggiustare le ultime due cifre
            loadSheetResultViewModel.OperatingWeight = loadSheetResultViewModel.DryOperatingWeight + loadSheetResultViewModel.TakeOffFuel;
            if (loadSheetResultViewModel.OperatingWeight <= 66990)
            {
                loadSheetResultViewModel.MaxPlacardWeight = 66990;
            }else if(loadSheetResultViewModel.OperatingWeight <= 69990)
            {
                loadSheetResultViewModel.MaxPlacardWeight = 69990;
            }else
            {
                loadSheetResultViewModel.MaxPlacardWeight = 74990;
            }
            loadSheetResultViewModel.Landing = fleetWeightData.FleetWeight.MaxLW + createLoadScheetModel.TripFuel;
            loadSheetResultViewModel.ZeroFuel = fleetWeightData.FleetWeight.MaxZFW + loadSheetResultViewModel.TakeOffFuel;
            loadSheetResultViewModel.AllowedWeightForTow = Math.Min(Math.Min(loadSheetResultViewModel.ZeroFuel, loadSheetResultViewModel.MaxPlacardWeight), loadSheetResultViewModel.Landing);
            loadSheetResultViewModel.AllowedTrafficLoad = loadSheetResultViewModel.AllowedWeightForTow - loadSheetResultViewModel.OperatingWeight;
            loadSheetResultViewModel.Pax = createLoadScheetModel.Adults + createLoadScheetModel.Childs;
            loadSheetResultViewModel.AdultNumber = createLoadScheetModel.Adults;
            loadSheetResultViewModel.ChildNumber = createLoadScheetModel.Childs;
            loadSheetResultViewModel.PaxForward = createLoadScheetModel.PaxForward;
            loadSheetResultViewModel.PaxMiddle = createLoadScheetModel.PaxMiddle;
            loadSheetResultViewModel.PaxAfter = createLoadScheetModel.PaxAfter;
            loadSheetResultViewModel.PaxForwardWeight = this._passengerWeightService.Execute(0, createLoadScheetModel.PaxForward).Weight;
            loadSheetResultViewModel.PaxMiddleWeight = this._passengerWeightService.Execute(1, createLoadScheetModel.PaxMiddle).Weight;
            loadSheetResultViewModel.PaxAfterWeight = this._passengerWeightService.Execute(2, createLoadScheetModel.PaxAfter).Weight;
            loadSheetResultViewModel.BagsHold1 = createLoadScheetModel.BagsHold1;
            loadSheetResultViewModel.BagsHold1Weight = this._loadsheetBagWeightService.Execute(1,createLoadScheetModel.BagsHold1).Weight;
            loadSheetResultViewModel.BagsHold2 = createLoadScheetModel.BagsHold2;
            loadSheetResultViewModel.BagsHold2Weight = this._loadsheetBagWeightService.Execute(2, createLoadScheetModel.BagsHold2).Weight;
            loadSheetResultViewModel.BagsHold3 = createLoadScheetModel.BagsHold3;
            loadSheetResultViewModel.BagsHold3Weight = this._loadsheetBagWeightService.Execute(3, createLoadScheetModel.BagsHold3).Weight;
            //loadSheetResultViewModel.BagsHold4 = createLoadScheetModel.BagsHold4;
            //loadSheetResultViewModel.BagsHold4Weight = this._loadsheetBagWeightService.Execute(4, createLoadScheetModel.BagsHold4).Weight;
            loadSheetResultViewModel.FreeGateBagsTrimAdjustement = 0; // TODO
            loadSheetResultViewModel.UncorrectLoad = loadSheetResultViewModel.PaxForwardWeight
                + loadSheetResultViewModel.PaxMiddleWeight
                + loadSheetResultViewModel.PaxAfterWeight
                + loadSheetResultViewModel.BagsHold1Weight
                + loadSheetResultViewModel.BagsHold2Weight
                + loadSheetResultViewModel.BagsHold3Weight
                + loadSheetResultViewModel.BagsHold4Weight
                + loadSheetResultViewModel.FreeGateBagsTrimAdjustement;

            loadSheetResultViewModel.Load = loadSheetResultViewModel.UncorrectLoad - loadSheetRepository.getChildAdjustment(createLoadScheetModel.Childs);
            if(loadSheetResultViewModel.Load <= loadSheetResultViewModel.AllowedTrafficLoad)
            {
                return View("Error");
            }
            loadSheetResultViewModel.ZeroFuelWeight = loadSheetResultViewModel.Load + loadSheetResultViewModel.DryOperatingWeight;
            loadSheetResultViewModel.Tow = loadSheetResultViewModel.ZeroFuelWeight + loadSheetResultViewModel.TakeOffFuel;
            return View(loadSheetResultViewModel);
        }


    }
}