﻿using System;
using VirtualDispatcher.Core;

namespace VirtualDispatcherWebsite.Models
{
    public class LoadSheetResultsViewModel
    {
        public int AdultNumber { get; internal set; }
        public double AllowedTrafficLoad { get; internal set; }
        public double AllowedWeightForTow { get; internal set; }
        public int BagsHold1 { get; internal set; }
        public double BagsHold1Weight { get; internal set; }
        public int BagsHold2 { get; internal set; }
        public double BagsHold2Weight { get; internal set; }
        public int BagsHold3 { get; internal set; }
        public double BagsHold3Weight { get; internal set; }
        public int BagsHold4 { get; internal set; }
        public double BagsHold4Weight { get; internal set; }
        public int ChildNumber { get; internal set; }
        public double DryOperatingWeight { get; internal set; }
        public int FreeGateBagsTrimAdjustement { get; internal set; }
        public int Landing { get; internal set; }
        public double Load { get; internal set; }
        public double MaxPlacardWeight { get; internal set; }
        public double OperatingWeight { get; internal set; }
        public int Pax { get; internal set; }
        public int PaxAfter { get; internal set; }
        public double PaxAfterWeight { get; internal set; }
        public int PaxForward { get; internal set; }
        public double PaxForwardWeight { get; internal set; }
        public int PaxMiddle { get; internal set; }
        public double PaxMiddleWeight { get; internal set; }
        public double TakeOffFuel { get; internal set; }
        public double Tow { get; internal set; }
        public double UncorrectLoad { get; internal set; }
        public double ZeroFuel { get; internal set; }
        public double ZeroFuelWeight { get; internal set; }
        //public FlightRoute flightRoute { get; set; }
        //public FleetWeightRegistrationData {get; set;}
    }
}