﻿using System;

namespace VirtualDispatcherWebsite.Models
{
    public class Flight
    {
        public string FlightNumber { get; set; }
        public string Destination { get; set; }
        public string Station { get; set; }
        public int CabinCrewNumber { get; set; }
        public string HostessCrewNumber { get; set; }
        public DateTime Flightdate { get; set; }
    }
}