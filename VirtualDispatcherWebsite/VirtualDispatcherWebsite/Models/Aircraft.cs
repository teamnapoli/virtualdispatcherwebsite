﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VirtualDispatcherWebsite.Models
{
    public class Aircraft
    {
        public string RegistrationNumber { get; set; }
        public string Weight { get; set; }
    }
}