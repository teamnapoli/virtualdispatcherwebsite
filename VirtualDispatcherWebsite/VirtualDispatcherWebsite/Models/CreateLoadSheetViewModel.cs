﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VirtualDispatcherWebsite.Models
{
    public class CreateLoadSheetViewModel
    {
        // weight on getAircraftByRegistration
        [Required]
        public double APSWeight { get; set; }

        public bool Observer1 { get; set; }

        public bool Observer2 { get; set; }

        public bool Attendance5 { get; set; }

        public bool Attendance6 { get; set; }
        [Required]
        public string MaxTakeOffWeight { get; set; } //todo : decimal

        // maxZfw on getAircraftByRegistration
        [Required]
        public int MaxZeroFuelWeight { get; set; }

        // maxLw on getAircraftByRegistration
        [Required]
        public int MaxLandingWeight { get; set; }
        [Required]
        public decimal CockpitCrewWeight { get; set; }
        [Required]
        public decimal CabinCrewWeight { get; set; }
        [Required]
        public string FlightNumber { get; set; }
        [Required]
        // pilot
        public int RampFuel { get; set; }
        [Required]
        //pilot
        public int TaxiFuel { get; set; }
        [Required]
        // pilot
        public int TripFuel { get; set; }
        [Required]
        public int MaxTakeOffWeightCaptain{ get; set; }
        [Required]
        public int Adults { get; set; }
        [Required]
        public int Childs { get; set; }
        [Required]
        public int Infant { get; set; }
        [Required]
        public int PaxForward { get; set; }
        [Required]
        public int PaxMiddle { get; set; }
        [Required]
        public int PaxAfter { get; set; }
        [Required]
        public int BagsHold1 { get; set; }
        [Required]
        public int BagsHold2 { get; set; }
        [Required]
        public int BagsHold3 { get; set; }
        [Required]
        public int BagsHold4 { get; set; }
        [Required]
        public decimal CargoHold1Weight { get; set; }
        [Required]
        public decimal CargoHold2Weight { get; set; }
        [Required]
        public decimal CargoHold3Weight { get; set; }
        [Required]
        public decimal CargoHold4Weight { get; set; }
        [Required]
        public string AircraftRegistrationNumber { get; set; }
    }
}