﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VirtualDispatcherWebsite.Models
{
    public class LoadSheetRepository
    {
        public double GetPaxForwardWeight(int paxForward)
        {
            return 420;
        }

        public double GetPaxMiddleWeight(int paxMiddle)
        {
            return 420;
        }

        public double GetPaxAfterWeight(int paxAfter)
        {
            return 420;
        }

        internal decimal GetBagsHold1Weight(decimal bagsHold1)
        {
            return 420;
        }

        internal decimal GetBagsHold2Weight(decimal bagsHold2)
        {
            return 420;
        }

        internal decimal GetBagsHold3Weight(decimal bagsHold3)
        {
            return 420;
        }

        internal decimal GetBagsHold4Weight(decimal bagsHold4)
        {
            return 420;
        }

        internal double getChildAdjustment(int childs)
        {
            return 420;
        }
    }
}