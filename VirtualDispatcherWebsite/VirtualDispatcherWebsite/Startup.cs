﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using VirtualDispatcher.Data;
using VirtualDispatcher.Service;

namespace VirtualDispatcherWebsite
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            //services
            services.AddScoped<IFlightRouteService, FlightRouteService>();
            services.AddScoped<ILoadSheetBagWeightService, LoadSheetBagWeightService>();
            services.AddScoped<IPassengerWeightService, PassengerWeightService>();
            services.AddScoped<IGetFleetWeightByRegistrationService, GetFleetWeightByRegistrationService>();
                
            //repositories
            services.AddScoped<ILoadSheetBagWeightRepository, LoadSheetBagWeightRepositoryMock>();
            services.AddScoped<IPassengerWeightRepository, PassengerWeightRepositoryMock>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
