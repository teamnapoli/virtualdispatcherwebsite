﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace VirtualDispatcher.Service
{
    public class HttpRequestUtility<T>
    {
        public T GetRequest(String uri)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
            httpWebRequest.Method = "GET";
            //Set the maximum redirection
            httpWebRequest.MaximumAutomaticRedirections = 4;
            httpWebRequest.MaximumResponseHeadersLength = 4;
            // Set credentials to use for this request.
            httpWebRequest.Credentials = CredentialCache.DefaultCredentials;
            //Set time out
            httpWebRequest.Timeout = 20000;
            Console.WriteLine("Sending HTTP Request");
            //Create a HTTP Web Response Instance
            var httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            //Get the Response Stream
            var responseStream = httpWebResponse.GetResponseStream();

            StreamReader streamReader = null;
            if (responseStream != null)
            {
                //Read Response Stream Using Stream Reader
                streamReader = new StreamReader(responseStream);
            }

            

            //Close Stream Response
            //if (responseStream != null) responseStream.Close();

            return JsonConvert.DeserializeObject<T>(streamReader.ReadToEnd());
        }
    }
}
