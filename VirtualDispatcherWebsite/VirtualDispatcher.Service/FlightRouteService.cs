﻿using System;
using System.Collections.Generic;
using VirtualDispatcher.Core;

namespace VirtualDispatcher.Service
{
    public class FlightRouteService : IFlightRouteService
    {
        public FlightRoute Execute(string flightNumber)
        {
            var apiGateway = new HttpRequestUtility<List<FlightRoute>>();

            var uri = "http://api.flightdispatcher.cloud/api/flightroutes?Search=" + flightNumber;

            return apiGateway.GetRequest(uri)[0];
        }
    }
}
