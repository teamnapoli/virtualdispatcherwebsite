﻿using System;
using System.Collections.Generic;
using System.Text;
using VirtualDispatcher.Core;
using VirtualDispatcher.Data;

namespace VirtualDispatcher.Service
{
    public class PassengerWeightService : IPassengerWeightService
    {
        private IPassengerWeightRepository _passengerWeightRepository;
        
        public PassengerWeightService(IPassengerWeightRepository passengerWeightRepository)
        {
            _passengerWeightRepository = passengerWeightRepository;
        }
        public LoadSheetPassengerWeight Execute(int location, int quantity)
        {
            return _passengerWeightRepository.GetItem(location, quantity);
        }
    }
}
