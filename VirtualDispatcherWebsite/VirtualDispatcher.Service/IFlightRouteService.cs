﻿using VirtualDispatcher.Core;

namespace VirtualDispatcher.Service
{
    public interface IFlightRouteService
    {
        FlightRoute Execute(string flightNumber);
    }
}