﻿using System;
using System.Collections.Generic;
using System.Text;
using VirtualDispatcher.Core;

namespace VirtualDispatcher.Service
{
    public class GetFleetWeightByRegistrationService : IGetFleetWeightByRegistrationService
    {
        public FleetWeightAircraftRegistration Execute(string aircraftRegistrationNumber)
        {
            var apiGateway = new HttpRequestUtility<FleetWeightAircraftRegistration>();

            var uri = "http://api.flightdispatcher.cloud/api/AirlineCompanies/375/FleetWeights/Aircrafts/" + aircraftRegistrationNumber;

            return apiGateway.GetRequest(uri);
        }
    }
}
