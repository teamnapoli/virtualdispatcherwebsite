﻿using VirtualDispatcher.Core;

namespace VirtualDispatcher.Service
{
    public interface IPassengerWeightService
    {
        LoadSheetPassengerWeight Execute(int location, int quantity);
    }
}