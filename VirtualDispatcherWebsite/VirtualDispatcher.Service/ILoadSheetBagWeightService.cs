﻿using VirtualDispatcher.Core;

namespace VirtualDispatcher.Service
{
    public interface ILoadSheetBagWeightService
    {
        LoadSheetBagWeight Execute(int hold, int quantity);
    }
}