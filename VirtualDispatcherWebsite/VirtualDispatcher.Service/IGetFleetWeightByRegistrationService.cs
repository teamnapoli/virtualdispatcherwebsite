﻿using VirtualDispatcher.Core;

namespace VirtualDispatcher.Service
{
    public interface IGetFleetWeightByRegistrationService
    {
        FleetWeightAircraftRegistration Execute(string aircraftRegistrationNumber);
    }
}