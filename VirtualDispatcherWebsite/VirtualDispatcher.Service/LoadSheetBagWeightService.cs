﻿using System;
using System.Collections.Generic;
using System.Text;
using VirtualDispatcher.Core;
using VirtualDispatcher.Data;

namespace VirtualDispatcher.Service
{
    public class LoadSheetBagWeightService : ILoadSheetBagWeightService
    {
        private ILoadSheetBagWeightRepository _loadBagRepository;

        public LoadSheetBagWeightService(ILoadSheetBagWeightRepository loadBagRepository)
        {
            _loadBagRepository = loadBagRepository;
        }

        public LoadSheetBagWeight Execute(int hold,int quantity)
        {
            return _loadBagRepository.GetItem(hold, quantity);
        }
    }
}
