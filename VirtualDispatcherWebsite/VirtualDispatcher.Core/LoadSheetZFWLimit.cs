﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace VirtualDispatcher.Core
{
    public class LoadSheetZFWLimit
    {
        [Required]
        public int ID { get; set; }

        [Required]
        public LoadSheet LoadSheet { get; set; }

        [Required]
        public double FromZFW { get; set; }

        [Required]
        public double ToZFW { get; set; }

        [Required]
        public double FromLimit { get; set; }

        [Required]
        public double ToLimit { get; set; }
    }
}
