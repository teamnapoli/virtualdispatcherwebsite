﻿using System;
using System.Collections.Generic;

namespace VirtualDispatcher.Core
{
    public class FleetWeight
    {

        public int ID { get; set; }

        public AirlineCompany AirlineCompany { get; set; }

        public string Name { get; set; }

        public DateTime Date { get; set; }

        public int Number { get; set; }

        public double StandardWeight { get; set; }

        public int MaxTOW { get; set; }

        public int MaxZFW { get; set; }

        public int MaxLW { get; set; }

        public int MaxTaxi { get; set; }

        public AircraftBrand AircraftModel { get; set; }

        public IList<FleetWeightOption> Options { get; set; }

        public IList<FleetWeightAircraftRegistration> Aircrafts { get; set; }
    }
}