﻿using System;

namespace VirtualDispatcher.Core
{
    public class AircraftModel
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string CodeIATA { get; set; }

        public string CodeICAO { get; set; }

        public string Seats { get; set; }

        public AircraftBrand AircraftBrand { get; set; }

        public AircraftWakeCategory AircraftWakeCategory { get; set; }
    }
}
