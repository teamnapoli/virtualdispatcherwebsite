﻿namespace VirtualDispatcher.Core
{
    public class AircraftBrand
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
    }
}