﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualDispatcher.Core
{
    public class FleetWeightAircraftRegistration
    {

        public int ID { get; set; }


        public FleetWeight FleetWeight { get; set; }

        public AircraftBrand AircraftModel { get; set; }

        public string Registration { get; set; }


        public double Weight { get; set; }

        public string Serial { get; set; }
    }
}
