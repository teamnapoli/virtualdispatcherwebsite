﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualDispatcher.Core
{
    public class LoadSheetBagWeight
    {
        public int ID { get; set; }

        public LoadSheet LoadSheet { get; set; }

        public string Hold { get; set; }

        public int Qty { get; set; }

        public double Weight { get; set; }
    }
}
