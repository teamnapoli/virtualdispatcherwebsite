﻿namespace VirtualDispatcher.Core
{
    public class FleetWeightOption
    {
        public int ID { get; set; }

        public FleetWeight FleetWeight { get; set; }

        public string Name { get; set; }

        public double WeightAdjustment { get; set; }
    }
}