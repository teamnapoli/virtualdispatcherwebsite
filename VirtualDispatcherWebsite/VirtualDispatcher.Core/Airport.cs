﻿namespace VirtualDispatcher.Core
{
    public class Airport
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string CodeIATA { get; set; }
        public string CodeICAO { get; set; }
    }
}