﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace VirtualDispatcher.Core
{
    public class LoadSheetFuelAdjustment
    {
        [Required]
        public int ID { get; set; }

        [Required]
        public LoadSheet LoadSheet { get; set; }

        [Required]
        public double FromWeight { get; set; }

        [Required]
        public double ToWeight { get; set; }

        [Required]
        public double Index { get; set; }
    }
}
