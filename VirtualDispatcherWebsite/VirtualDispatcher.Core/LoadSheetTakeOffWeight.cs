﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace VirtualDispatcher.Core
{
    public class LoadSheetTakeOffWeight
    {
        [Required]
        public int ID { get; set; }

        [Required]
        public LoadSheet LoadSheet { get; set; }

        [Required]
        public double FromWeight { get; set; }

        [Required]
        public double ToWeight { get; set; }

        [Required]
        public double FromLimit { get; set; }

        [Required]
        public double ToLimit { get; set; }

        [Required]
        public int MaxPlacard { get; set; }
    }
}
