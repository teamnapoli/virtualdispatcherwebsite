﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualDispatcher.Core
{
    public class FlightRoute
    {
        public int ID { get; set; }
        public AirlineCompany AirlineCompany { get; set; }
        public string Number { get; set; }
        public Airport FromAirport { get; set; }
        public Airport ToAirport { get; set; }
    }
}
