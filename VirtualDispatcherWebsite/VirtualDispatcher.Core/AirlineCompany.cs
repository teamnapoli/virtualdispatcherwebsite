﻿namespace VirtualDispatcher.Core
{
    public class AirlineCompany
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string BaseAirport { get; set; }
        public string Country { get; set; }
        public int Start { get; set; }
        public string CodeIATA { get; set; }
        public string CodeICAO { get; set; }
        public string CodeAWB { get; set; }
    }
}