﻿namespace VirtualDispatcher.Core
{
    public class LoadSheet
    {
        public int ID { get; set; }

        public AirlineCompany AirlineCompany { get; set; }

        public AircraftBrand AircraftModel { get; set; }

        public string Description { get; set; }

        public double FerryExtensionLimit { get; set; }

        public string Color { get; set; }
    }
}