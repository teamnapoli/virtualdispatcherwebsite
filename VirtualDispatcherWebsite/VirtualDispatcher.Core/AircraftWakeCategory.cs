﻿namespace VirtualDispatcher.Core
{
    public class AircraftWakeCategory
    {
        public int ID { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }
    }
}