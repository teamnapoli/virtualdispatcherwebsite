﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace VirtualDispatcher.Core
{
    public class LoadSheetPassengerWeight
    {
        [Required]
        public int ID { get; set; }

        [Required]
        public LoadSheet LoadSheet { get; set; }

        [Required]
        [MaxLength(50, ErrorMessageResourceName = "LoadSheetPassengerWeight_Location_ErrorMessage")]
        public string Location { get; set; }

        [Required]
        public int Qty { get; set; }

        [Required]
        public double Weight { get; set; }
    }
}
