﻿using System.Collections.Generic;
using VirtualDispatcher.Core;

namespace VirtualDispatcher.Data
{
    public interface IPassengerWeightRepository
    {
        void Add(LoadSheetPassengerWeight Item);
        bool Delete(int ID);
        LoadSheetPassengerWeight GetItem(int location, int quantity);
        IList<LoadSheetPassengerWeight> GetItems(string Search);
        bool Save();
        void Update(LoadSheetPassengerWeight Item);
    }
}