﻿using System.Collections.Generic;
using VirtualDispatcher.Core;

namespace VirtualDispatcher.Data
{
    public interface ILoadSheetBagWeightRepository
    {
        void Add(LoadSheetBagWeight Item);
        bool Delete(int ID);
        LoadSheetBagWeight GetItem(int hold, int quantity);
        IList<LoadSheetBagWeight> GetItems(string Search);
        bool Save();
        void Update(LoadSheetBagWeight Item);
    }
}